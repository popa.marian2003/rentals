﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inchirieri.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public int RegDate { get; set; }
        public string Fuel { get; set; }
        public int Seats { get; set; }
        public string Color { get; set; }
        public string Gearbox { get; set; }
        public int Price { get; set; }
    }
}